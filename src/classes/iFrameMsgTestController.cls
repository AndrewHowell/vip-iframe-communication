//==============================================================================================
// Apex Class: iFrameMsgTestController 
// Controller for 'iFrameMsgTestPage' VF Page and 'iFrameMsgTestComponent' lightning component.
//==============================================================================================
public with sharing class iFrameMsgTestController 
{
    public static String orgVFDomain {get; private set;}
    public static String orgSFDomain {get; private set;}
    public static String orgLtngDomain {get; private set;}
    
    // NOTE: 'URL.getSalesforceBaseUrl().toExternalForm()' returns different values, based 
    // 		on whether being instantiated as a Visualforce Controller, or being called as 
    // 		a controller action from Lightning. Because of this, I use a static initializer, 
    // 		but replace the values in the constructor (only called on controller instatiation 
    // 		from Visualforce Page).
    		
    // Static Initializer -> This is valid if being called by Lightning 
    static
    {
        // -- Build From Lightning Side -- //
        // Build Salesforce Domain
        String sfInstance = 'na3';
        String sfDomain = URL.getSalesforceBaseUrl().toExternalForm();	// e.g. 'https://agh-dev-ed.my.salesforce.com'
        iFrameMsgTestController.orgSFDomain = String.valueof(sfDomain);
        system.debug('iFrameMsgTestController Static Initializer - Salesforce Domain: '+ orgSFDomain);
        // Build Visualforce Domain
        String customDomain = sfDomain.split('.my.')[0];		// e.g. 'https://agh-dev-ed'
        system.debug('iFrameMsgTestController Static Initializer - Custom Domain (segment): '+ customDomain);
        String vfDomain = customDomain + '--c.' + sfInstance + '.visual.force.com'; // e.g. 'https://agh-dev-ed--c.na3.visual.force.com'
        iFrameMsgTestController.orgVFDomain = String.valueof(vfDomain);
        system.debug('iFrameMsgTestController Static Initializer - Salesforce Domain: '+ orgVFDomain);	
        // Build Lightning Domain		
        String ltngDomain = customDomain + '.lightning.force.com';	// e.g. 'https://agh-dev-ed.lightning.force.com'
        iFrameMsgTestController.orgLtngDomain = String.valueof(ltngDomain);
        system.debug('iFrameMsgTestController Static Initializer - Lightning Domain: '+ orgLtngDomain);  
    }
    // Constructor -> Replaces static values -> This is valid if being called by Visualforce Page
    public iFrameMsgTestController()
    {
        // -- Build from Visualforce Side -- //
        // Build Visualforce Domain
        String vfDomain = URL.getSalesforceBaseUrl().toExternalForm();	// e.g. 'https://agh-dev-ed--c.na3.visual.force.com'
        iFrameMsgTestController.orgVFDomain = String.valueof(vfDomain);
        system.debug('iFrameMsgTestController Constructor - Visualforce Domain: '+ orgVFDomain);
        // Build Salesforce Domain
        String customDomain = vfDomain.split('--')[0];		// e.g. 'https://agh-dev-ed'
        system.debug('iFrameMsgTestController Constructor - Custom Domain (segment): '+ customDomain);
        String sfDomain = customDomain + '.salesforce.com'; // e.g. 'https://agh-dev-ed.salesforce.com'
        iFrameMsgTestController.orgSFDomain = String.valueof(sfDomain);
        system.debug('iFrameMsgTestController Constructor - Salesforce Domain: '+ orgSFDomain);	
        // Build Lightning Domain		
        String ltngDomain = customDomain + '.lightning.force.com';	// e.g. 'https://agh-dev-ed.lightning.force.com'
        iFrameMsgTestController.orgLtngDomain = String.valueof(ltngDomain);
        system.debug('iFrameMsgTestController Constructor - Lightning Domain: '+ orgLtngDomain);   
    }

    // ========================== PUBLIC METHODS ========================== //
    
    //===========================================================
    // Method: fetchMapConfig()
    // Returns a MapConfigWrapper object
    @AuraEnabled
    public static iFrameMsgConfigWrapper fetchMapConfig()
    {
        iFrameMsgConfigWrapper theMapConfig = new iFrameMsgConfigWrapper();
        theMapConfig.orgVisualforceDomain = orgVFDomain;
        theMapConfig.orgSalesforceDomain = orgSFDomain; 
        theMapConfig.orgLightningDomain = orgLtngDomain;
        system.debug('iFrameMsgTestController.fetchMapConfig() - Visualforce Domain: '+ orgVFDomain);
        system.debug('iFrameMsgTestController.fetchMapConfig() - Salesforce Domain: '+ theMapConfig.orgSalesforceDomain);
        system.debug('iFrameMsgTestController.fetchMapConfig() - Lightning Domain: '+ theMapConfig.orgLightningDomain);
        return theMapConfig;
    }
    
    
	// =========================== SUB CLASSES =========================== // 

	//==========================================================
    // Class: iFrameMsgConfigWrapper
    public class iFrameMsgConfigWrapper 
    {
        @AuraEnabled public String orgVisualforceDomain{get;set;}   
        @AuraEnabled public String orgSalesforceDomain{get;set;}
        @AuraEnabled public String orgLightningDomain{get;set;}
    }
	   
}