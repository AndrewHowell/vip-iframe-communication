# README #

PURPOSE:
This repository contains a Lightning App, containing a Lightning Component with an embedded iFrame, encapsulating a Visualforce page. The component communicates with the enclosed page via Window.postMessage().

NOTE: This will NOT work if clickjack protection is enabled for customer Visualforce pages with headers disabled. Setup -> Security Controls -> Session Settings -> 'Enable clickjack protection for customer Visualforce pages with headers disabled.'

TO USE:
  * Deploy included files to an org
  * Open the Developer Console and open Lightning Resource "iFrameMsgTestApp"
  * Click Preview.