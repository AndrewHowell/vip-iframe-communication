//=========================================================================================
// File: iFrameMsgTestComponentHelper.js
// Author: Andrew Howell (Vertiba)
//=========================================================================================
({
    //======================================================================
    // Method: initHelper (component)
    initHelper : function(component)
    {
        // -- Get config from controller -- //
        var configAction = component.get('c.fetchMapConfig'); 
        configAction.setCallback(this, function(response) 
        {
            var state = response.getState();
            // If return successfully,
            if (component.isValid() && state === "SUCCESS")
            {
                // Get the config wrapper
                var iFrameMsgConfigWrapper = response.getReturnValue(); 
                if (this.isVarDefined(iFrameMsgConfigWrapper))
                {
                    // -- Get the config items we need -- //
                    if (this.isVarDefined(iFrameMsgConfigWrapper.orgLightningDomain))
                    	{component.set('v.Org_Lightning_Domain',iFrameMsgConfigWrapper.orgLightningDomain);}
                    if (this.isVarDefined(iFrameMsgConfigWrapper.orgVisualforceDomain))
                    	{component.set('v.Org_Visualforce_Domain',iFrameMsgConfigWrapper.orgVisualforceDomain);}
                    if (this.isVarDefined(iFrameMsgConfigWrapper.orgSalesforceDomain))
                    	{component.set('v.Org_Salesforce_Domain',iFrameMsgConfigWrapper.orgSalesforceDomain);}
                    component.set('v._status', component.get('v._status') + '<p class="actionMsg">(2) ... Recieved Config Info from Controller.</p><br/>');
                    // -- Now add iFrame Init Handler -- //
                    component.set('v._status', component.get('v._status') + '<p>(3) Adding Initialization Message Handler...</p>');
                    this.addIFrameInitHandler(component);
                } 
            }
            else if (state === "ERROR") // Handle any error by reporting it
            {
                for (var error in response.getError())
                    {console.log('ERROR: ' + error.message);}
            }
        });
        // Execute the action
        component.set('v._status', component.get('v._status') + '<p class="actionMsg">(1) Requesting Config Info from Controller...</p>');
        $A.enqueueAction(configAction); 
    },
    
    //======================================================================
    // Method: addIFrameInitHandler (component)
    // Adds an Event Listener to the iFrame to listen for a page initialized event.
    addIFrameInitHandler: function(component)
    {
        var currStatus;
        var sendCoordMsgToIFrameFunc = this.sendCoordMsgToIFrame;
        
        // -- Listen for Message from iFrame that Map is Ready -- //
    	window.addEventListener('message',$A.getCallback(
        function (postInitEvt)
    	{
        	if (postInitEvt && postInitEvt.data === 'Init_Complete' && component.isValid())
            { 
                // Check the origin
                var expectedOrigin = decodeURIComponent(component.get('v.Org_Visualforce_Domain'));
                var originDomain = (postInitEvt.origin || postInitEvt.originalEvent.origin); // For Chrome, the origin property is in the event.originalEvent object.
            	if (originDomain === expectedOrigin)
                {
                    // -- Receieved Init Message -- //
                    component.set('v._status', component.get('v._status') + '<p class="iFrameMsg">(6) ... Received Initialization Message from iFrame Window.</p><br/>');
                    
                    // -- Now Send a Message Back -- //
                    var coord = [40.01274705277217,-105.27788230486078];
                    component.set('v._status', component.get('v._status') + "<p>Now let's send a reply with coordinates...</p>");
                    sendCoordMsgToIFrameFunc(component,coord);
                } 
                else
                {console.log('iFrameMsgTestComponentHelper.addIFrameInitHandler() - FAILED SECURITY CHECK FROM IFRAME - Message: "' + postInitEvt.data + '" Origin Domain: ' + originDomain + ' Expected Origin: ' + expectedOrigin);}  
            }
    	}),false);
        component.set('v._status', component.get('v._status') + '<p>(4) ... Done Adding Initialization Message Handler. Waiting for message...</p><br/>');
    },
    
    //===========================================================
    // Method: sendCoordMsgToIFrame (component,coordinate)
    sendCoordMsgToIFrame : function(component,coordinate)
    {
		var mapIFrame = component.find("mapIFrame").getElement();
        var theContentWindow = mapIFrame.contentWindow;
        var targetDomain = decodeURIComponent(component.get('v.Org_Visualforce_Domain'));
        var msg = 'Set_Loc:'+ coordinate.toString();
        component.set('v._status', component.get('v._status') + '<p class="iFrameMsg">(7) Sending iFrame request to set location to [' + coordinate.toString() + '] ...</p>');            
        theContentWindow.postMessage(msg,targetDomain);
    },
    
    //===========================================================
    // Method: isVarDefined(varToCheck)
    isVarDefined: function(varToCheck) 
    {
        return ((varToCheck !== undefined && varToCheck !== null)?true:false);
    },

})